// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "fight_gameGameMode.h"
#include "fight_gameCharacter.h"
#include "UObject/ConstructorHelpers.h"

Afight_gameGameMode::Afight_gameGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
