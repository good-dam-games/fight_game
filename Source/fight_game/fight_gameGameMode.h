// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "fight_gameGameMode.generated.h"

UCLASS(minimalapi)
class Afight_gameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Afight_gameGameMode();
};



