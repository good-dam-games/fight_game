// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "fight_game/fight_gameGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodefight_gameGameMode() {}
// Cross Module References
	FIGHT_GAME_API UClass* Z_Construct_UClass_Afight_gameGameMode_NoRegister();
	FIGHT_GAME_API UClass* Z_Construct_UClass_Afight_gameGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_fight_game();
// End Cross Module References
	void Afight_gameGameMode::StaticRegisterNativesAfight_gameGameMode()
	{
	}
	UClass* Z_Construct_UClass_Afight_gameGameMode_NoRegister()
	{
		return Afight_gameGameMode::StaticClass();
	}
	struct Z_Construct_UClass_Afight_gameGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_Afight_gameGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_fight_game,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_Afight_gameGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "fight_gameGameMode.h" },
		{ "ModuleRelativePath", "fight_gameGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_Afight_gameGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<Afight_gameGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_Afight_gameGameMode_Statics::ClassParams = {
		&Afight_gameGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_Afight_gameGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_Afight_gameGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_Afight_gameGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_Afight_gameGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(Afight_gameGameMode, 1816695016);
	template<> FIGHT_GAME_API UClass* StaticClass<Afight_gameGameMode>()
	{
		return Afight_gameGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_Afight_gameGameMode(Z_Construct_UClass_Afight_gameGameMode, &Afight_gameGameMode::StaticClass, TEXT("/Script/fight_game"), TEXT("Afight_gameGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(Afight_gameGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
