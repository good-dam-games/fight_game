// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIGHT_GAME_fight_gameGameMode_generated_h
#error "fight_gameGameMode.generated.h already included, missing '#pragma once' in fight_gameGameMode.h"
#endif
#define FIGHT_GAME_fight_gameGameMode_generated_h

#define fight_game_Source_fight_game_fight_gameGameMode_h_12_SPARSE_DATA
#define fight_game_Source_fight_game_fight_gameGameMode_h_12_RPC_WRAPPERS
#define fight_game_Source_fight_game_fight_gameGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define fight_game_Source_fight_game_fight_gameGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAfight_gameGameMode(); \
	friend struct Z_Construct_UClass_Afight_gameGameMode_Statics; \
public: \
	DECLARE_CLASS(Afight_gameGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/fight_game"), FIGHT_GAME_API) \
	DECLARE_SERIALIZER(Afight_gameGameMode)


#define fight_game_Source_fight_game_fight_gameGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAfight_gameGameMode(); \
	friend struct Z_Construct_UClass_Afight_gameGameMode_Statics; \
public: \
	DECLARE_CLASS(Afight_gameGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/fight_game"), FIGHT_GAME_API) \
	DECLARE_SERIALIZER(Afight_gameGameMode)


#define fight_game_Source_fight_game_fight_gameGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	FIGHT_GAME_API Afight_gameGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Afight_gameGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(FIGHT_GAME_API, Afight_gameGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Afight_gameGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	FIGHT_GAME_API Afight_gameGameMode(Afight_gameGameMode&&); \
	FIGHT_GAME_API Afight_gameGameMode(const Afight_gameGameMode&); \
public:


#define fight_game_Source_fight_game_fight_gameGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	FIGHT_GAME_API Afight_gameGameMode(Afight_gameGameMode&&); \
	FIGHT_GAME_API Afight_gameGameMode(const Afight_gameGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(FIGHT_GAME_API, Afight_gameGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Afight_gameGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(Afight_gameGameMode)


#define fight_game_Source_fight_game_fight_gameGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define fight_game_Source_fight_game_fight_gameGameMode_h_9_PROLOG
#define fight_game_Source_fight_game_fight_gameGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	fight_game_Source_fight_game_fight_gameGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	fight_game_Source_fight_game_fight_gameGameMode_h_12_SPARSE_DATA \
	fight_game_Source_fight_game_fight_gameGameMode_h_12_RPC_WRAPPERS \
	fight_game_Source_fight_game_fight_gameGameMode_h_12_INCLASS \
	fight_game_Source_fight_game_fight_gameGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define fight_game_Source_fight_game_fight_gameGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	fight_game_Source_fight_game_fight_gameGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	fight_game_Source_fight_game_fight_gameGameMode_h_12_SPARSE_DATA \
	fight_game_Source_fight_game_fight_gameGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	fight_game_Source_fight_game_fight_gameGameMode_h_12_INCLASS_NO_PURE_DECLS \
	fight_game_Source_fight_game_fight_gameGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FIGHT_GAME_API UClass* StaticClass<class Afight_gameGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID fight_game_Source_fight_game_fight_gameGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
